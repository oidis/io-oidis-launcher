/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::XCppCommons::Utils::ArgsParser;
namespace po = boost::program_options;

namespace Io::Oidis::Launcher::Structures {
    TEST(LauncherArgsTest, getTarget) {
        LauncherArgs launcherArgs;

        ASSERT_STREQ("", launcherArgs.getTarget().c_str());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--target=resources/target")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);
        ASSERT_STREQ("resources/target", launcherArgs.getTarget().c_str());
    }

    TEST(LauncherArgsTest, getPort) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(80, launcherArgs.getPort());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--port=8080")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);
        ASSERT_EQ(8080, launcherArgs.getPort());
    }

    TEST(LauncherArgsTest, setRuntimeEnv_browser) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--runtime-env=bRowSer")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_EQ(true,
                  launcherArgs.getOpenBrowser());
        ASSERT_EQ(false,
                  launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() || launcherArgs.getConnectorStart() ||
                  launcherArgs.getConnectorRestart() || launcherArgs.getConnectorStop());
    }

    TEST(LauncherArgsTest, setRuntimeEnv_wuiconnector) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--runtime-env=wuiconnector")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_EQ(true,
                  launcherArgs.getConnectorStart());
        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorRestart() || launcherArgs.getConnectorStop());
    }

    TEST(LauncherArgsTest, setRuntimeEnv_wuihost) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--runtime-env=wuihost")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_EQ(true,
                  launcherArgs.getRunHost() && launcherArgs.getConnectorStart());
        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorRestart() || launcherArgs.getConnectorStop());
    }

    TEST(LauncherArgsTest, setRuntimeEnv_wuichromiumre) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--runtime-env=wuichromiumre")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_EQ(true,
                  launcherArgs.getRunChromiumRE() && launcherArgs.getConnectorStart());
        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop() || launcherArgs.getRunHost());
    }

    TEST(LauncherArgsTest, connectorRestart) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--restart")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_EQ(true,
                  launcherArgs.getConnectorRestart());
        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorStop() || launcherArgs.getRunHost());
    }

    TEST(LauncherArgsTest, connectorStop) {
        LauncherArgs launcherArgs;

        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunHost() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorStart() || launcherArgs.getConnectorRestart() ||
                  launcherArgs.getConnectorStop());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--stop")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_EQ(true,
                  launcherArgs.getConnectorStop());
        ASSERT_EQ(false,
                  launcherArgs.getOpenBrowser() || launcherArgs.getRunChromiumRE() ||
                  launcherArgs.getConnectorRestart() || launcherArgs.getConnectorStart() || launcherArgs.getRunHost());
    }

    TEST(LauncherArgsTest, connectorElevate) {
        LauncherArgs launcherArgs;
        ASSERT_FALSE(launcherArgs.IsConnectorElevate());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--elevate-connector")};
        ArgsParser::Parse(launcherArgs, 2, (const char **) argv);

        ASSERT_TRUE(launcherArgs.IsConnectorElevate());
    }
}  // namespace Io::Oidis::Launcher::Structures
