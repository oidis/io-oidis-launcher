/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_LAUNCHER_TEST_SOURCEFILESMAP_HPP_  // NOLINT
#define IO_OIDIS_LAUNCHER_TEST_SOURCEFILESMAP_HPP_

#include "../../../reference.hpp"

#endif  // IO_OIDIS_LAUNCHER_TEST_SOURCEFILESMAP_HPP_  NOLINT
