/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef IO_OIDIS_LAUNCHER_INTERFACESMAP_HPP_  // NOLINT
#define IO_OIDIS_LAUNCHER_INTERFACESMAP_HPP_

namespace Io {
    namespace Oidis {
        namespace Launcher {
            class Application;
            class Loader;
            namespace Structures {
                class LauncherArgs;
            }
            namespace Utils {
                class EnvironmentHandler;
            }
        }
    }
}

#endif  // IO_OIDIS_LAUNCHER_INTERFACESMAP_HPP_  // NOLINT
