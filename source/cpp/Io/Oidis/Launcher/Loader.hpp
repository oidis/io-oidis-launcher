/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_LAUNCHER_LOADER_HPP_
#define IO_OIDIS_LAUNCHER_LOADER_HPP_

namespace Io::Oidis::Launcher {
    /**
     * Loader class provides static application methods.
     */
    class Loader : public Io::Oidis::XCppCommons::Loader {
     public:
        /**
         * Contains application logic entry method.
         * @param $argc An argument count.
         * @param $argv An array of arguments.
         * @return Returns exit code.
         */
        static int Load(const int $argc, const char *$argv[]) {
            Io::Oidis::Launcher::Application application;
            return Io::Oidis::XCppCommons::Loader::Load(application, $argc, $argv);
        }
    };
}

#endif  // IO_OIDIS_LAUNCHER_LOADER_HPP_
