/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_LAUNCHER_APPLICATION_HPP_
#define IO_OIDIS_LAUNCHER_APPLICATION_HPP_

namespace Io::Oidis::Launcher {
    class Application : public Io::Oidis::XCppCommons::Application {
     public:
        int Run(int $argc, const char **$argv) override;

     private:
        using IpcFallbackAction = std::function<void(unsigned int)>;

        /**
         * Sends message to connector.
         * @param $appTarget An application target path.
         * @param $connectorName Specify actual WuiConnector name.
         * @param $args Specify messages to be send.
         * @return Returns true if sent succeed, false otherwise.
         */
        static bool sendMessageToConnector(const string &$appTarget, const string &$connectorName, const std::vector<string> &$args);

        /**
         * Sends message to connector.
         * @param $appTarget An application target path.
         * @param $connectorName Specify actual WuiConnector name.
         * @param $message Specify message to be send.
         * @return Returns true if send succeed, false otherwise.
         */
        static bool sendMessageToConnector(const string &$appTarget,
                                           const string &$connectorName,
                                           const string &$message,
                                           IpcFallbackAction $ipcFallbackAction = nullptr);

        /**
         * Starts child process.
         * @param $cmd Specify command to start. If not found in $cwd then searching in system PATH is used internally.
         * @param $arguments Specify arguments for command.
         * Note that if $cmd is empty than first argument is expected as command.
         * @param $cwd Specify working directory or use current if $cwd is empty (by default).
         * @param $isElevated Set to true if process should run as elevated.
         * @return Returns 0 if failed or PID of created process.
         */
        static unsigned int processStart(const string &$cmd, const std::vector<string> &$arguments = {},
                                         const string &$cwd = "", bool $isElevated = false);

        /**
         * Opens target application in browser which is configured as default in system.
         * @param $target A target application path to open.
         */
        static void openDefaultBrowser(const string &$target);

        /**
         * Opens new instance of WuiConnector.
         * @param $connectorName Specify actual WuiConnector name.
         * @param $parameters Specify parameters to start connector with.
         * @param $connectorDir Specify directory where connector is located.
         * @param $args Specify arguments for connector application (forwarded from Launcher --app-args=*).
         * @param $isElevated Set to true if connector should run as elevated process.
         */
        static void openConnector(const string &$connectorName, const std::vector<string> &$parameters, const string &$connectorDir,
                                  const string &$args, bool $isElevated = false);

        /**
         * Opens target application in WuiChromiumRE.
         * @param $connectorName Specify actual WuiConnector name.
         * @param $target A target application path to open.
         * @param $args Specify arguments for chromiumRE application (forwarded from Launcher --app-args=*).
         */
        static void openChromiumRE(const string &$connectorName, const string &$target, const string &$args);

        /**
         * Launch target application in WuiNodejsRE.
         * @param $connectorName Specify actual WuiConnector name.
         * @param $target A target application path to open.
         */
        static void runNodejsRE(const string $connectorName, const string &$target);

        /**
         * Loads LauncherArgs from <appName>.config file expected in executable directory.
         * @param $args [out] Loaded args.
         */
        static void loadFromAppConfig(Io::Oidis::Launcher::Structures::LauncherArgs &$args);

        /**
         * Splits arguments '--arg="arg value with "quotes" --here." --arg2=123' into array and correct quotes escaping in the same step.
         * So result looks like ['--arg="arg value with \"quotes\" --here."', '--arg2=123'] (single quotes denotes string type not value).
         * @param $args An argument string to split and quote.
         * @return Returns processed arguments in array.
         */
        static std::vector<string> splitAndQuoteArgs(const string &$args);

        static void terminateAllConnectorInstances(const string &$connectorName);

        static void terminateConnectorInstance(unsigned int $pid);
    };
}

#endif  // IO_OIDIS_LAUNCHER_APPLICATION_HPP_
