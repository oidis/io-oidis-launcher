/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_LAUNCHER_STRUCTURES_LAUNCHERARGS_HPP_
#define IO_OIDIS_LAUNCHER_STRUCTURES_LAUNCHERARGS_HPP_

namespace Io::Oidis::Launcher::Structures {
    /**
     * LauncherArgs class provides program options used in WuiLauncher application.
     */
    class LauncherArgs
            : public Io::Oidis::XCppCommons::Structures::ProgramArgs {
     public:
        /**
         * Constructs args instance with configuration of all program options.
         */
        LauncherArgs();

        /**
         * @return Returns target application path.
         */
        const string &getTarget() const;

        /**
         * @param $value Specify target application path.
         */
        void setTarget(const string &$value);

        /**
         * @return Returns port.
         */
        int getPort() const;

        /**
         * @param $value Specify port.
         */
        void setPort(int $value);

        /**
         * @return Returns actual WuiConnector name.
         */
        const string &getConnectorName() const;

        /**
         * @param $value Specify actual WuiConnector name.
         */
        void setConnectorName(const string &$value);

        /**
         * @return Returns true if connector restart is required, false otherwise.
         */
        bool getConnectorRestart() const;

        /**
         * @param $value Specify request to restart connector.
         */
        void setConnectorRestart(bool $value);

        /**
         * @return Returns true if connector stop is required, false otherwise.
         */
        bool getConnectorStop() const;

        /**
         * @param $value Specify request to stop connector.
         */
        void setConnectorStop(bool $value);

        /**
         * @return Returns true if connector start is required, false otherwise.
         */
        bool getConnectorStart() const;

        /**
         * @param $value Specify request to start connector.
         */
        void setConnectorStart(bool $value);

        /**
         * @return Returns true if target should be opened in default browser, false otherwise.
         */
        bool getOpenBrowser() const;

        /**
         * @param $value Specify request to open target in default browser.
         */
        void setOpenBrowser(bool $value);

        /**
         * @return Returns true if target should be opened in host browser, false otherwise.
         */
        bool getRunHost() const;

        /**
         * @param $value Specify request to run target in host.
         */
        void setRunHost(bool $value);

        /**
         * @return Returns true if run target in native WUI runtime environment is required.
         */
        bool getRunChromiumRE() const;

        /**
         * @param $value Specify request to run target in native WUI runtime environment.
         */
        void setRunChromiumRE(bool $value);

        /**
         * @param $value Specify requested runtime environment type.
         */
        void setRuntimeEnvironment(const string &$value);

        /**
         * @return Returns true if connector should Run As, false otherwise.
         */
        bool IsConnectorElevate() const;

        /**
         * @param $connectorElevate Set true to run connector with administrator rights.
         */
        void setConnectorElevate(bool $connectorElevate);

        /**
         * @return Returns true if connector should run as part of plugin RE, false otherwise.
         */
        bool IsPluginRE() const;

        /**
         * @param $value Set true to run connector as part of plugin RE otherwise false.
         */
        void setPluginRE(bool $value);

        /**
         * @return Returns true if target should be launched in Nodejs WUI runtime environment.
         */
        bool getRunNodejsRE() const;

        /**
         * @param $value Specify request to launch target in Nodejs WUI runtime environment.
         */
        void setRunNodejsRE(bool $value);

        /**
         * @return Returns arguments string for application. This application arguments will be <B>emplaced back</B> to CLI arguments
         * for launched application, ignored for openBrowser RE.
         * Ensure correct format of arguments i.e. --app-args="--target="<path to target>" --connector-port=<port>".
         */
        const string &getAppArgs() const;

        /**
         * @param $appArgs Specify arguments string for application. This application arguments will be <B>emplaced back</B>
         * to CLI arguments for launched application, ignored for openBrowser RE.
         * Ensure correct format of arguments i.e. --app-args="--target="<path to target>" --connector-port=<port>".
         */
        void setAppArgs(const string &$appArgs);

        /**
         * @return Returns true if application should run in single instance.
         */
        bool IsSingleton() const;

        /**
         * @param $singleton Specify true to require single application instance.
         */
        void setSingleton(bool $singleton);

        friend std::ostream &operator<<(std::ostream &$os, const LauncherArgs &$args);

     private:
        string target = "";
        int port = 80;
        string connectorName = "WuiConnector";
        string appArgs = "";
        bool connectorRestart = false;
        bool connectorStart = false;
        bool connectorStop = false;
        bool openBrowser = false;
        bool runHost = false;
        bool runChromiumRE = false;
        bool connectorElevate = false;
        bool isPluginRE = false;
        bool runNodejsRE = false;
        bool singleton = false;
    };
}

#endif  // IO_OIDIS_LAUNCHER_STRUCTURES_LAUNCHERARGS_HPP_
