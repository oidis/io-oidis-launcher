/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_LAUNCHER_UTILS_ENVIRONMENTHANDLER_HPP_
#define IO_OIDIS_LAUNCHER_UTILS_ENVIRONMENTHANDLER_HPP_

namespace Io::Oidis::Launcher::Utils {
    /**
     * EnvironmentHandler class provides static methods to handle environment variables or registry keys in the case of Windows OS.
     */
    class EnvironmentHandler {
     public:
        /**
         * Get list of registry values. Only 'string' value type is now supported.
         * @param $key Specify registry key.
         * @param $options Optional options. Specify {wow: 64} to access x64 registry, or {wow: 32} to access x86 registry.
         * System default WOW type is used if not specified.
         * @return Returns list of each registry values for specified key or empty if no value found or error.
         */
        static std::vector<string> getRegistryValues(const string &$key, const json &$options = json());

        /**
         * Get single registry value. Only 'string' value type is now supported.
         * @param $key Specify registry key.
         * @param $name Specify registry value name in specified key.
         * @param $options Optional options. Specify {wow: 64} to access x64 registry, or {wow: 32} to access x86 registry.
         * System default WOW type is used if not specified.
         * @return Returns registry value or empty string if not found or not succeed.
         */
        static string getRegistryValue(const string &$key, const string &$name, const json &$options = json());

        /**
         * Get map of environment variables of current process runtime context or global if required.
         * @param $options Optional options. Specify {global: true} to update environment variables from OS storage before.
         * @return Returns map <environment-key, value> with each environment variables or empty if no variable found or not succeed.
         */
        static std::map<string, string> getEnvironmentVariables(const json &$options = json());

        /**
         * Get environment variable of current process runtime context or global if required.
         * @param $name Specify requesting environment key.
         * @param $options Optional options. Specify {global: true} to update environment variables from OS storage before.
         * @return Returns environment value or empty string if not found or not succeed.
         */
        static string getEnvironmentVariable(const string &$name, const json &$options = json());

        EnvironmentHandler() = delete;

        EnvironmentHandler(EnvironmentHandler const &) = delete;

        void operator=(EnvironmentHandler const &)= delete;
    };
}

#endif  // IO_OIDIS_LAUNCHER_UTILS_ENVIRONMENTHANDLER_HPP_
