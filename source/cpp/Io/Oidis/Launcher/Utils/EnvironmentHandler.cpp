/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Io::Oidis::Launcher::Utils::EnvironmentHandler;

using Io::Oidis::XCppCommons::Utils::LogIt;
using Io::Oidis::XCppCommons::Primitives::String;
using boost::filesystem::exists;
using boost::filesystem::path;

#define REG_MAX_VALUE_NAME 16383

#ifdef WIN_PLATFORM

HKEY getRegistryHandle(const string &$key) {
    HKEY returnValue = nullptr;
    string key = boost::to_upper_copy($key);
    if (boost::iequals(key, "HKLM") || boost::iequals(key, "HKEY_LOCAL_MACHINE")) {
        returnValue = HKEY_LOCAL_MACHINE;
    } else if (boost::iequals(key, "HKCU") || boost::iequals(key, "HKEY_CURRENT_USER")) {
        returnValue = HKEY_CURRENT_USER;
    } else if (boost::iequals(key, "HKCR") || boost::iequals(key, "HKEY_CLASSES_ROOT")) {
        returnValue = HKEY_CLASSES_ROOT;
    } else if (boost::iequals(key, "HKU") || boost::iequals(key, "HKEY_USERS")) {
        returnValue = HKEY_USERS;
    } else if (boost::iequals(key, "HKCC") || boost::iequals(key, "HKEY_CURRENT_CONFIG")) {
        returnValue = HKEY_CURRENT_CONFIG;
    } else {
        LogIt::Error("This registry key \"{0}\" is undefined.", key);
    }
    return returnValue;
}

REGSAM getRegistryOpenFlags(const REGSAM $default, const json &$options) {
    REGSAM regFlags = $default;
    if (!$options.empty()) {
        if ($options.value("wow", 0) == 64) {
            regFlags |= KEY_WOW64_64KEY;
        } else if ($options.value("wow", 0) == 32) {
            regFlags |= KEY_WOW64_32KEY;
        }
    }
    return regFlags;
}

#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
extern char **environ;
#endif

void updateEnvFromRegistry() {
#ifdef WIN_PLATFORM
    const string HKLM = "HKLM/SYSTEM/CurrentControlSet/Control/Session Manager/Environment";
    const string HKCU = "HKCU/Environment";
    std::map<string, string> currentEnv;
    std::vector<string> localMachineEnv = EnvironmentHandler::getRegistryValues(HKLM);
    std::vector<string> userEnv = EnvironmentHandler::getRegistryValues(HKCU);

    LogIt::Info("Updating environment variables from registry. HKLM/HKCU : {0}/{1}", localMachineEnv.size(), userEnv.size());

    std::for_each(localMachineEnv.begin(), localMachineEnv.end(), [&](const string &$item) {
        if (!boost::iequals(String::ToLowerCase($item), "path")) {
            currentEnv.emplace($item, EnvironmentHandler::getRegistryValue(HKLM, $item));
        }
    });
    std::for_each(userEnv.begin(), userEnv.end(), [&](const string &$item) {
        if (currentEnv.find($item) != currentEnv.end()) {
            currentEnv[$item] = EnvironmentHandler::getRegistryValue(HKCU, $item);
        } else {
            if (!boost::iequals(String::ToLowerCase($item), "path")) {
                currentEnv.emplace($item, EnvironmentHandler::getRegistryValue(HKCU, $item));
            }
        }
    });
    auto cmdIgnoreCasePath = [](const string &item) -> bool {
        return boost::iequals(String::ToLowerCase(item), "path");
    };

    string pathVal;
    auto found = std::find_if(localMachineEnv.begin(), localMachineEnv.end(), cmdIgnoreCasePath);
    if (found != localMachineEnv.end()) {
        pathVal += EnvironmentHandler::getRegistryValue(HKLM, *found);
        LogIt::Debug("PATH updated in current process environment to: {0}", pathVal);
    } else {
        LogIt::Error("PATH variable not found in local machine environment!!!");
    }
    found = std::find_if(userEnv.begin(), userEnv.end(), cmdIgnoreCasePath);
    if (found != userEnv.end()) {
        if (!pathVal.empty()) {
            pathVal += ";";
        }
        pathVal += EnvironmentHandler::getRegistryValue(HKCU, *found);
        LogIt::Debug("PATH updated in current process environment to: {0}", pathVal);
    } else {
        LogIt::Error("PATH variable not found in local machine environment!!!");
    }

    currentEnv.emplace("PATH", pathVal);

    std::for_each(currentEnv.begin(), currentEnv.end(), [](std::pair<const string, string> &$item) {
        auto *tmp = new char[8192];
        DWORD size = ExpandEnvironmentStrings($item.second.c_str(), tmp, 8192);
        if (size > 8192) {
            delete[] tmp;
            tmp = new char[size];
            size = ExpandEnvironmentStrings($item.second.c_str(), tmp, size);
        }

        if (size != 0) {
            $item.second = string(tmp, size);
        } else {
            LogIt::Error("Can not expand registry variable {0}={1}", $item.first, $item.second);
        }

        delete[] tmp;
    });

    std::for_each(currentEnv.begin(), currentEnv.end(), [&](std::pair<const string, string> &item) {
        LogIt::Debug("set variable: {0}={1}", item.first.c_str(), item.second.c_str());
        if (SetEnvironmentVariable(item.first.c_str(), item.second.c_str()) == 0) {
            LogIt::Error("Can not set environment variable {0}={1}, error: {2}", item.first, item.second,
                         static_cast<int> (GetLastError()));
        }
    });

    LogIt::Info("Process environment variables {0} updated.", currentEnv.size());
#else
    LogIt::Info("Function 'updateEnvFromRegistry' not implemented on this platform.");
#endif
}

std::vector<string> EnvironmentHandler::getRegistryValues(const string &$key, const json &$options) {
#ifdef WIN_PLATFORM
    std::vector<string> retVal;
    if (!$key.empty()) {
        string key = boost::replace_all_copy($key, "/", "\\");
        unsigned int keyIndex = key.find_first_of('\\');
        if (keyIndex > 0 && keyIndex < key.size()) {
            string regGroup = boost::to_upper_copy(key.substr(0, keyIndex));
            key = key.substr(keyIndex + 1);

            HKEY reg = getRegistryHandle(regGroup);
            if (reg != nullptr &&
                RegOpenKeyEx(reg, key.c_str(), 0, getRegistryOpenFlags(KEY_QUERY_VALUE, $options), &reg) == ERROR_SUCCESS) {
                DWORD cchClassName = MAX_PATH, cValues, cchMaxValue, cbMaxValueData, cchValue;
                LONG retCode;

                retCode = RegQueryInfoKey(reg, nullptr, &cchClassName, nullptr, nullptr, nullptr, nullptr, &cValues, &cchMaxValue,
                                          &cbMaxValueData, nullptr, nullptr);
                if (retCode == ERROR_SUCCESS) {
                    if (cValues != 0u) {
                        TCHAR achValue[REG_MAX_VALUE_NAME];
                        for (DWORD i = 0; i < cValues; i++) {
                            cchValue = REG_MAX_VALUE_NAME;
                            achValue[0] = '\0';
                            retCode = RegEnumValue(reg, i, achValue, &cchValue, nullptr, nullptr, nullptr, nullptr);

                            if (retCode == ERROR_SUCCESS) {
                                string str(achValue, cchValue);
                                retVal.push_back(str);
                            }
                        }
                    }
                }

                RegCloseKey(reg);
            } else {
                LogIt::Error("Requested registry key \"{0}\" can not be opened.", $key);
            }
        }
    } else {
        LogIt::Error("Requested registry key must not be empty.");
    }
    return retVal;
#else
    LogIt::Info("Function 'getRegistryValues' not implemented on this platform.");
    return std::vector<string>();
#endif
}

string EnvironmentHandler::getRegistryValue(const string &$key, const string &$name, const json &$options) {
#ifdef WIN_PLATFORM
    string retVal;
    if (!$key.empty() && !$name.empty()) {
        string key = boost::replace_all_copy($key, "/", "\\");
        unsigned int keyIndex = key.find_first_of('\\');
        if (keyIndex > 0 && keyIndex < key.size()) {
            string regGroup = boost::to_upper_copy(key.substr(0, keyIndex));
            key = key.substr(keyIndex + 1);

            HKEY reg = getRegistryHandle(regGroup);
            if (reg != nullptr &&
                RegOpenKeyEx(reg, key.c_str(), 0, getRegistryOpenFlags(KEY_QUERY_VALUE, $options), &reg) == ERROR_SUCCESS) {
                DWORD dataLength = 8192;
                DWORD bufferSize = 8192;
                auto *buffer = reinterpret_cast<char *>(malloc(bufferSize));
                LONG status;

                status = RegQueryValueEx(reg, $name.c_str(), nullptr, nullptr, (LPBYTE) buffer, &dataLength);
                while (status == ERROR_MORE_DATA) {
                    bufferSize += 4096;
                    dataLength = bufferSize;
                    auto *newBuffer = reinterpret_cast<char *>(realloc(buffer, bufferSize));
                    if (newBuffer != nullptr) {
                        buffer = newBuffer;
                        status = RegQueryValueEx(reg, $name.c_str(), nullptr, nullptr, (LPBYTE) buffer, &dataLength);
                    } else {
                        status = ERROR_CREATE_FAILED;
                        break;
                    }
                }

                if (status == ERROR_SUCCESS) {
                    retVal = string(buffer);
                } else {
                    LogIt::Error("Query for registry value failed.");
                }

                free(buffer);

                RegCloseKey(reg);
            } else {
                LogIt::Error("Requested registry key \"{0}\" can not be opened.", $key);
            }
        }
    } else {
        LogIt::Error("Requested registry key must not be empty.");
    }
    return retVal;
#else
    LogIt::Info("Function 'getRegistryValue' not implemented on this platform.");
    return string();
#endif
}

std::map<string, string> EnvironmentHandler::getEnvironmentVariables(const json &$options) {
#ifdef WIN_PLATFORM
    std::map<string, string> retVal;
    if (!$options.empty() && $options.value("global", false)) {
        updateEnvFromRegistry();
    }

    LPTCH allEnvs = GetEnvironmentStrings();
    if (allEnvs != nullptr) {
        LPTSTR lpszVariable;
        for (lpszVariable = (LPTSTR) allEnvs; *lpszVariable != 0; lpszVariable++) {
            string key, value;
            bool second = false;
            while (*lpszVariable != 0) {
                if (!second) {
                    if (*lpszVariable != '=') {
                        key.append(1, *lpszVariable);
                    } else {
                        second = true;
                    }
                } else {
                    value.append(1, *lpszVariable);
                }
                lpszVariable++;
            }

            if (!key.empty()) {
                retVal.emplace(key, value);
            }
        }
        FreeEnvironmentStrings(allEnvs);
    } else {
        LogIt::Error("Environment variables map is empty for that process.");
    }
    return retVal;
#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
    int i = 1;
    char *s = *environ;
    std::map<string, string> retVal;

    for (; s != nullptr; i++) {
        s = *(environ + i);
        if (s == nullptr) {
            continue;
        }
        string line(s);
        auto envVar = String::Split(line, {"="});

        string key, value;
        if (envVar.size() > 0) {
            key = envVar[0];
        }
        if (envVar.size() > 1) {
            value = envVar[1];
        }
        retVal.emplace(key, value);
    }
    return retVal;
#endif
}

string EnvironmentHandler::getEnvironmentVariable(const string &$name, const json &$options) {
    string retVal;
    if (!$options.empty() && $options.value("permanent", false)) {
        updateEnvFromRegistry();
    }

#ifdef WIN_PLATFORM
    auto *tmp = new char[8192];
    DWORD size = GetEnvironmentVariable($name.c_str(), tmp, 8192);
    if (size > 8192) {
        delete[] tmp;
        tmp = new char[size];
        size = GetEnvironmentVariable($name.c_str(), tmp, size);
    }

    if (size != 0) {
        retVal = string(tmp, size);
    } else {
        LogIt::Debug("Environment variable value for {0} not exists.", $name);
    }

    delete[] tmp;
    tmp = nullptr;

#else
    char *val = ::getenv($name.c_str());
    if (val != nullptr) {
        retVal = string(val);
    }
#endif
    return retVal;
}
