/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 * Copyright (c) 2019 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef IO_OIDIS_LAUNCHER_HPP_  // NOLINT
#define IO_OIDIS_LAUNCHER_HPP_

#include <unistd.h>

#include "../../dependencies/com-wui-framework-xcppcommons/source/cpp/reference.hpp"
#include "interfacesMap.hpp"
#include "Io/Oidis/Launcher/sourceFilesMap.hpp"

// global-using-start
using std::vector;
// global-using-stop

// generated-code-start
#include "Io/Oidis/Launcher/Application.hpp"
#include "Io/Oidis/Launcher/Structures/LauncherArgs.hpp"
#include "Io/Oidis/Launcher/Utils/EnvironmentHandler.hpp"
// generated-code-end


#include "Io/Oidis/Launcher/Loader.hpp"

#endif  // IO_OIDIS_LAUNCHER_HPP_  NOLINT
